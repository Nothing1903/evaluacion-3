/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.evaluacion3;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import root.alumno.dao.AlumnoJpaController;
import root.evaluacion3.dao.exceptions.NonexistentEntityException;
import root.evaluacion3.entity.Alumno;

/**
 *
 * @author itorres
 */
@Path("alumnos")
public class Alumnos {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarSolicitudes() {
        AlumnoJpaController dao = new AlumnoJpaController();

        List<Alumno> lista = dao.findAlumnoEntities();

        return Response.ok(200).entity(lista).build();

    }

    @GET
    @Path("/{idbuscar}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response consultarPorRut(@PathParam("idbuscar") String idbusca) {

        AlumnoJpaController dao = new AlumnoJpaController();

        Alumno alumno = dao.findAlumno(idbusca);
       System.out.print("rut devuelto"+alumno.getRut());
        return Response.ok(200).entity(alumno).build();

    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response add(Alumno alumno) {

        try {
            AlumnoJpaController dao = new AlumnoJpaController();
            dao.create(alumno);
        } catch (Exception ex) {
            Logger.getLogger(Alumno.class.getName()).log(Level.SEVERE, null, ex);
        }

        return Response.ok(200).entity(alumno).build();

    }

    @DELETE
    @Path("/{iddelete}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response delete(@PathParam("iddelete") String iddelete) {

        try {
            AlumnoJpaController dao = new AlumnoJpaController();

            dao.destroy(iddelete);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(Alumno.class.getName()).log(Level.SEVERE, null, ex);
        }

        return Response.ok("alumno eliminado").build();
    }

    @PUT
    public Response update(Alumno alumno) {

        try {
            AlumnoJpaController dao = new AlumnoJpaController();
            dao.edit(alumno);
        } catch (Exception ex) {
            Logger.getLogger(Alumno.class.getName()).log(Level.SEVERE, null, ex);
        }

        return Response.ok(200).entity(alumno).build();
    }

}
