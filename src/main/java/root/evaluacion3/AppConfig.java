package root.evaluacion3;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * Configures JAX-RS for the application.
 * @author itorres
 */
@ApplicationPath("api")
public class AppConfig extends Application {
    
}
