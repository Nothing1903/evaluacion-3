<%-- 
    Document   : index
    Created on : 02-05-2021, 21:25:34
    Author     : itorres

--%>


<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="root.evaluacion3.entity.Alumno"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
      List<Alumno> lista   = (List<Alumno>) request.getAttribute("lista");
    Iterator<Alumno> itAlumno = lista.iterator();
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <form  name="form" action="AlumnoController" method="POST">
           <div class="cover-container d-flex h-100 p-3 mx-auto flex-column">
            
               
               <table border="1">
                    <thead>
                    <th>Rut</th>
                    <th>Nombre </th>
                    <th>Fecha Nacimiento </th><!-- comment -->
                    <th>correo </th>
                    <th> </th>
        
                    </thead>
                    <tbody>
                        <%while (itAlumno.hasNext()) {
                       Alumno per = itAlumno.next();%>
                        <tr>
                            <td><%= per.getRut()%></td>
                            <td><%= per.getNombre()%></td>
                            <td><%= per.getFechaNacimiento()%></td>
                            <td><%= per.getCorreo()%></td>
                          
                 <td> <input type="radio" name="seleccion" value="<%= per.getRut()%>"> </td>
                
                        </tr>
                        <%}%>                
                    </tbody>           
                </table>
              <button type="submit" name="accion" value="ver" class="btn btn-success">ver alumno</button>
            <button type="submit" name="accion" value="eliminar" class="btn btn-success">eliminar</button>

        
        </form>
    </body>
</html>
